from digi.xbee.devices import XBeeDevice
from discover import Discover
from send import Send
from recieve import Recieve


class Controller():

    def setup(self, device, baud_rate):
        self.device = device
        self.baud_rate = baud_rate
        self.initdev(self.device, self.baud_rate)

    def initdev(self, device, baud_rate):
        self.xbee = XBeeDevice(device, baud_rate)
        self.xbee.open()

    def discovering(self):
        self.disc = Discover()
        nodes = self.disc.discover(self.xbee)
        return nodes

    def senddata(self, reciever, data):
        self.send = Send()
        self.send.send(self.xbee, reciever, data)

    def senddatahex(self, reciever, data):
        self.send = Send()
        self.send.sendHex(self.xbee, reciever, data)

    def startrecieve(self):
        self.recieve = Recieve()
        self.recieve.recieve(self.xbee)

    def getrecieved(self):
        returntext = self.recieve.getRecieved()
        return returntext
