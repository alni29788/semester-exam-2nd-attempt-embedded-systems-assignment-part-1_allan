****# Semester Exam - 2nd attempt-Embedded Systems UCL - assignment part 1_Allan_Nielsen ****



**Embedded Systems - assignment part 1 **

Sub:
Students are required to build a simple 802.15.4 network between two nodes. A Coordinator and an End Point.
The original setup needs to be done using XCTU or a python script.
Afterwards students will connect the End Point on their RPi and the Coordinator will remain on their working computer.
A simple script should be executed on the RPi while the Coordinator will be running serial via XTCU or a python script.
Communication between the two nodes will be bidirectional. 
