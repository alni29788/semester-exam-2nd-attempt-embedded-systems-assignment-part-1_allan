import time


class Discover():

    def discover(self, xbee):
        self.xbee = xbee
        nodes = []
        try:
            self.xnet = self.xbee.get_network()

            self.xnet.start_discovery_process()
            while self.xnet.is_discovery_running():
                time.sleep(0.5)

            prenodes = self.xnet.get_devices()
            nodes = {}
            for node in prenodes:
                nodes[node.get_node_id()] = (node)

        except:
            pass

        return nodes
