from PyQt5 import QtWidgets
from PyQt5.QtCore import QTimer
from form import Ui_Main
from controller import Controller
import sys


class myWindow(Ui_Main):

    def __init__(self, window):
        self.setupUi(window)
        self.btnDisc.clicked.connect(self.clickDiscover)
        self.btnRecieve.clicked.connect(self.clickRecieve)
        self.btnSend.clicked.connect(self.clickSend)
        self.controller = Controller()
        # self.controller.setup("/dev/ttyUSB0", 9600)
        self.controller.setup("COM4", 9600)

    def clickDiscover(self):
        self.nodes = self.controller.discovering()
        self.cmbList.clear()
        self.cmbList.addItem("")
        for x in self.nodes:
            self.cmbList.addItem(self.nodes[x].get_node_id())
        self.cmbList.currentIndexChanged.connect(self.changeReciever)

    def clickRecieve(self):
        self.controller.startrecieve()
        self.timer = QTimer()
        self.timer.setInterval(100)
        self.timer.setSingleShot(False)
        self.timer.timeout.connect(self.getRecieved)
        self.timer.start(100)
        self.txtRecieve.append("Started recieving")

    def getRecieved(self):
        newText = self.controller.getrecieved()
        if(newText != ""):
            self.txtRecieve.append("Message recieved: "+newText)
            #Atomated massage hack
            self.controller.senddata ("C1E1","Return automated massage")
    def changeReciever(self):
        chosen = str(self.cmbList.currentText())
        if(chosen.strip() != ""):
            node = self.nodes[chosen]
            self.txtName.setText(node.get_node_id())
            self.txtHex.setText(str(node.get_64bit_addr()))

    def clickSend(self):
        if(self.txtHex.text() != ""):
            self.controller.senddatahex(self.txtHex.text(), self.txtSend.toPlainText())
        else:
            self.controller.senddata(self.txtName.text(), self.txtSend.toPlainText())
        self.txtRecieve.append("Message sent to "+self.txtName.text()+": "+self.txtSend.toPlainText())
        return ""


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QWidget()
    ui = myWindow(MainWindow)
    MainWindow.show()
    app.exec_()
