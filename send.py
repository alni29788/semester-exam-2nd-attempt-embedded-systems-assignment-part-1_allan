from digi.xbee.devices import XBee64BitAddress
from digi.xbee.devices import RemoteXBeeDevice


class Send():

    def send(self, xbee, reciever, data):
        try:
            self.xbee = xbee
            self.xnet = self.xbee.get_network()
            remote_device = self.xnet.discover_device(reciever)
            self.xbee.send_data(remote_device, data)
        except:
            pass

    def sendHex(self, xbee, reciever, data):
        try:
            self.xbee = xbee
            remote = RemoteXBeeDevice(self.xbee, XBee64BitAddress.from_hex_string(reciever))
            self.xbee.send_data(remote, data)
        except:
            pass
