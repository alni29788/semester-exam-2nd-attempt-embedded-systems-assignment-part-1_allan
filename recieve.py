from datetime import datetime
class Recieve():

    def __init__(self):
        self.recieved = ""

    def recieve(self, xbee):
        self.xbee = xbee
        self.xbee.add_data_received_callback(self.data_recieve_callback)

    def data_recieve_callback(self, xbee_message):
        nodeid = xbee_message.remote_device.get_node_id()
        message = xbee_message.data.decode()
        self.recieved += datetime.now().strftime('%H:%M:%S')+" - "+nodeid+": "+message
        #print("Message recieved: "+ self.recieved)

    def getRecieved(self):
        returntext = self.recieved
        self.recieved = ""
        return returntext
